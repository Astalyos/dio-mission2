-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  sam. 14 déc. 2019 à 18:47
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `api`
--

-- --------------------------------------------------------

--
-- Structure de la table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `identifian` int(11) NOT NULL,
  `commune` varchar(255) NOT NULL,
  `elem_patri` varchar(255) NOT NULL,
  `elem_princ` text NOT NULL,
  `departement` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `fields`
--

INSERT INTO `fields` (`id`, `identifian`, `commune`, `elem_patri`, `elem_princ`, `departement`) VALUES
(1, 75056160, 'Paris', 'Pont Alexandre III', 'Pont en arc Ã  tablier portÃ© en acier moulÃ© peint. 4 colonnes carrÃ©es supportant des statues dorÃ©es.', 75),
(2, 75056174, 'Paris', 'Pont Saint-Louis', 'Pont-poutres en acier, rambardes Ã  barreaudage mÃ©tallique.', 75),
(3, 75056165, 'Paris', 'Pont des Arts', 'Pont en fonte, en arc Ã  tablier portÃ©.', 75),
(4, 78267150, 'Gargenville', 'Pont de Rangiport', 'Pont Ã  poutres latÃ©rales Ã  treillis mÃ©tallique.', 78),
(5, 78124151, 'CarriÃ¨res-sur-Seine', 'Pont ferroviaire', 'Pont-poutres en bÃ©ton.', 78),
(6, 95018150, 'Argenteuil', 'Pont-aqueduc de Colombes', 'Pont-poutres cantilever en mÃ©tal.', 95),
(7, 92004151, 'AsniÃ¨res-sur-Seine', 'Pont ferroviaire', 'Large pont-poutres mÃ©tallique, piles en pierre.', 92),
(8, 94027151, 'Athis-Mons', 'Pont de Chemin de Fer', 'Pont-poutres Ã  treillis mÃ©tallique et piles en pierre', 94),
(9, 77288150, 'Melun', 'Pont de la PÃ©nÃ©trante', 'Pont-poutres en bÃ©ton', 77),
(10, 77288154, 'Melun', 'Pont Notre-Dame', 'Pont-poutres en bÃ©ton', 77),
(11, 77305150, 'Montereau-Fault-Yonne', 'Pont de Seine', 'Pont voÃ»tÃ© en pierre Ã  arches surbaissÃ©es', 77),
(12, 77305152, 'Montereau-Fault-Yonne', 'Pont ferroviaire', 'Pont-poutres en bÃ©ton', 77),
(13, 77079153, 'Champagne-sur-Seine', 'Passerelle - aqueduc de la Voulzie', 'Passerelle en arc Ã  tablier intermÃ©diaire, en \"bow string\" en bÃ©ton armÃ© - Longueur : 250m', 77),
(14, 94340151, 'Joinville-le-Pont', 'Pont de joinville', 'Pont en arc Ã  tablier portÃ© en bÃ©ton', 94),
(15, 93050152, 'Neuilly-sur-Marne', 'Pont de la N370', 'Pont-poutres en bÃ©ton Ã  deux travÃ©es, tablier mÃ©tallique, culÃ©e en pierres de taille, piles doubles', 93),
(16, 77276150, 'Mareuil-lÃ¨s-Meaux', 'Viaduc de Meaux', 'Haut et long viaduc Ã  poutres en bÃ©ton, courbe, Ã  nombreuses travÃ©es et en lÃ©gÃ¨re pente', 77),
(17, 77284152, 'Meaux', 'Pont du MarchÃ©', 'Pont voÃ»tÃ© en pierre et enduit en amont et remplissage briques en aval, Ã  arcs en plein cintre Ã  7 travÃ©es, avant-becs', 77),
(18, 77265151, 'Luzancy', 'Pont de la D402', 'Pont-poutres Ã  une travÃ©e en bÃ©ton, tablier mÃ©tallique', 77),
(19, 77441150, 'Samois-sur-Seine', 'Front de berge', 'Grandes maisons de diffÃ©rents styles entourÃ©es de leur parc', 77),
(20, 77464150, 'Thorigny-sur-Marne', 'Front de berge', 'Ensemble de villas prÃ©cÃ©dÃ©es de jardinets clos. Villas en meuliÃ¨re et de style anglo-normand notamment.', 77),
(21, 77463151, 'Thomery', 'Port', 'En pente douce vers la Seine, port pavÃ©e', 77),
(22, 75056152, 'Paris', 'Pont Mirabeau', 'Pont en arc mÃ©tallique, piles en pierre qui reprÃ©sentent des bateaux avec des statues allÃ©goriques et armoiries de la ville de Paris.', 75),
(23, 75056167, 'Paris', 'Pont au Change', 'Pont voÃ»tÃ© en pierre Ã  arcs surbaissÃ©s. EmblÃªme de NapolÃ©on au-dessus de chacune des piles.', 75),
(24, 78672150, 'Villennes-sur-Seine', 'Pont de Villennes', 'Pont-poutres avec avant-becs massifs.', 78),
(25, 78396150, 'Le Mesnil-le-Roi', 'Passerelle sur le Petit Bras', 'Pont-poutres, voie en stabilisÃ©', 78),
(26, 78092151, 'Bougival', 'Pont marÃ©chal de Lattre-de-Tassigny', 'Pont-poutres en bÃ©ton.', 78),
(27, 78146150, 'Chatou', 'Pont ferroviaire', 'Pont en deux sections : pont rive gauche en arcs en pierres ; pont-poutres mÃ©tallique rive droite.', 78),
(28, 78124152, 'CarriÃ¨res-sur-Seine', 'Pont ferroviaire des Anglais', 'Pont-poutres mÃ©tallique, piles en pierres.', 78),
(29, 92024150, 'Clichy-la-Garenne', 'Pont de Gennevilliers', 'Pont en arc Ã  arches mÃ©talliques ; piles en pierres.', 92),
(30, 92012152, 'Boulogne-Billancourt', 'Pont de SÃ¨vres', 'Pont-poutres en acier et dalles en bÃ©ton - Longueur : 150m.', 92),
(31, 91521150, 'Ris-Orangis', 'Pont de Champrosay', 'Pont-poutres Ã  piles de forme pyramidale inversÃ©e, en bÃ©ton et tablier mÃ©tallique', 91),
(32, 77079150, 'Champagne-sur-Seine', 'Pont (D 301)', 'Pont-poutres en bÃ©ton', 77),
(33, 77079151, 'Champagne-sur-Seine', 'Pont de Saint-MammÃ¨s', 'Pont-poutres en bÃ©ton', 77),
(34, 77210150, 'La Grande-Paroisse', 'Pont ferroviaire', 'Pont en bow-string (arcs et cordes) en bÃ©ton armÃ©, arches en acier.', 77),
(35, 77279150, 'Marolles-sur-Seine', 'Pont ferroviaire', 'Pont-poutres en bÃ©ton', 77),
(36, 77051150, 'Bray-sur-Seine', 'Pont de la D412', 'Pont-poutres en bÃ©ton', 77),
(37, 93049150, 'Neuilly-Plaisance', 'Viaduc de Neuilly-Plaisance', 'Pont-poutres en bÃ©ton Ã  deux travÃ©es dont une enjambant la voie', 93),
(38, 93033150, 'Noisy-le-Grand', 'Pont de Gournay', 'Pont voÃ»tÃ© Ã  trois travÃ©es ; tablier mÃ©tallique, piles en bÃ©ton', 93),
(39, 93033151, 'Noisy-le-Grand', 'Passerelle de Champs', 'Passerelle mÃ©tallique en arc Ã  deux travÃ©es', 93),
(40, 77183150, 'La FertÃ©-sous-Jouarre', 'Pont Jean JaurÃ¨s', 'Pont-poutres Ã  deux travÃ©es en bÃ©ton', 77),
(41, 77265150, 'Luzancy', 'Pont ferroviaire', 'Pont-poutres', 77),
(42, 78624151, 'Triel-sur-Seine', 'TÃªte du Vieux Pont', '2 immeubles Ã  angle Ã  pan coupÃ© R+1 et R+1+C.', 78),
(43, 78092152, 'Bougival', 'Front de berge', 'Immeubles avec commerces en RDC, en pierres de taille et enduit, quelques immeubles mansardÃ©es.', 78),
(44, 78267151, 'Gargenville (Rangiport)', 'Front de berge', 'Au nÂ°16, villÃ©giature de type anglo-normande', 78),
(45, 78498154, 'Poissy', 'Front de berge', 'A noter au nÂ°41, maison de bourg ayant pignon sur rue Ã  pas de moineaux ; au nÂ°43, maison de bourg avec tourelle triangulaire sur la faÃ§ade ; au nÂ°45, maisons de bourg Ã  faÃ§ade Ã  pans de bois avec lodgia', 78),
(46, 75056151, 'Paris', 'Pont du Garigliano', 'Pont-poutres mÃ©talliques, piles en bÃ©ton.', 75),
(47, 75056186, 'Paris', 'Pont Amont', 'Pont-poutres en bÃ©ton Ã  trois travÃ©es.', 75),
(48, 78089150, 'BonniÃ¨res-sur-Seine', 'Pont de BonniÃ¨res', 'Pont-poutres Ã  tablier mÃ©tallique, larges piles en bÃ©ton, rambardes mÃ©talliques. Le pont est surÃ©levÃ© par rapport Ã  la ville de BonniÃ¨res, offrant un point de vue sur la ville.', 78),
(49, 78361150, 'Mantes-la-Jolie', 'Pont Neuf de Mantes', 'Pont-poutres mÃ©tallique et larges piles en bÃ©ton cÃ´tÃ© Mantes ; en pierre cÃ´tÃ© Limay, Ã  arches en plein cintre.', 78),
(50, 78335150, 'Limay', 'Pont de Limay', 'Pont voÃ»tÃ© en pierre, reste 7 arcs en plein cintre.', 78),
(51, 78624150, 'Triel-sur-Seine', 'Viaduc suspendu de Triel', 'Pont suspendu Ã  chaÃ®nes Ã  tablier mÃ©tallique et pylÃ´nes en bÃ©ton', 78),
(52, 78498153, 'Poissy', 'Pont de Poissy', 'Pont en arc Ã  tablier portÃ© en mÃ©tal (structure apparente) - Longueur : 185m. Permet le franchissement de la D 190', 78),
(53, 78172150, 'Conflans-Sainte-Honorine', 'Viaduc ferroviaire Joly', 'Pont-poutres Ã  tablier mÃ©tallique', 78),
(54, 78358150, 'Maisons-Laffitte', 'Pont de Maisons-Laffitte', 'Pont-poutres en bÃ©ton armÃ©. Permet le franchissement de la D 308', 78),
(55, 95063150, 'Bezons', 'Pont de Bezons', 'Pont en arc Ã  tablier portÃ© mÃ©tallique, piles en bÃ©ton armÃ©. Pont routier, tramway T2 sur partie centrale.', 95),
(56, 92026152, 'Courbevoie', 'Pont de Courbevoie', 'Pont-poutres en bÃ©ton', 92),
(57, 92073150, 'Suresnes', 'Pont de Suresnes', 'Pont-poutres Ã  arches surbaissÃ©es en bÃ©ton', 92),
(58, 94041150, 'Ivry-sur Seine', 'Passerelle Ivry-Charenton', 'Passerelle-poutres cantilever, en bÃ©ton', 94),
(59, 77236150, 'Jaulnes', 'Barrage-Ã©cluse de Jaulnes', 'Barrage en bÃ©ton Ã  trois travÃ©es, Ã©cluse accolÃ©e', 77),
(60, 94046151, 'Maisons-Alfort', 'Viaduc ferroviaire de Charenton-le-Pont', 'Pont-poutres Ã  deux travÃ©es, piles en bÃ©ton, tablier mÃ©tallique', 94),
(61, 94046152, 'Maisons-Alfort', 'Pont de Charenton', 'Pont-poutres en bÃ©ton Ã  une travÃ©e', 94),
(62, 94068150, 'Saint-Maur-des-FossÃ©s', 'Passerelle du Hallage', 'Pont-poutres en bÃ©ton Ã  une travÃ©e, lÃ©gÃ¨rement bombÃ© en son centre ; sortie piÃ©tons et cycle rive gauche par deux rampes et un escalier ; un escalier et une rampe rive droite', 94),
(63, 94068156, 'Saint-Maur-des-FossÃ©s', 'Pont de ChenneviÃ¨res', 'Pont-poutres large en bÃ©ton, Ã  trois travÃ©es, piles doubles', 94),
(64, 94052150, 'Nogent-sur-Marne', 'Pont de Nogent', 'Pont-poutres Ã  deux travÃ©es, tablier mÃ©tallique double ; piles en forme de Y en bÃ©ton. DÃ©cor : sculpture fÃ©minine sur la pile centrale incarnant la Marne du sculpteur Gaston Cadenat', 94),
(65, 94015151, 'Bry-sur-Marne', 'Passerelle de Bry', 'Pont-poutres Ã  trois travÃ©es, accessible par un escalier Ã  double entrÃ©e ; piles en pierre, tablier mÃ©tallique Ã  poutres apparentes', 94),
(66, 77231152, 'Isles-les-Meldeuses', 'Pont de la D17', 'Pont-poutres Ã  trois travÃ©es en bÃ©ton', 77),
(67, 77397150, 'SaÃ¢cy-sur-Marne', 'Pont ferroviaire', 'Pont-poutres en bÃ©ton Ã  trois travÃ©es', 77),
(68, 93039151, 'Saint-Denis', 'Front de berge', 'Deux immeubles en briques, deux en enduit colorÃ© et pierres de taille. Riches dÃ©cors en faÃ§ade.', 93),
(69, 94019150, 'ChenneviÃ¨res-sur-Marne', 'Front de berge', 'Nombreuses grandes villas ayant jardin sur Marne, de styles variÃ©s', 94),
(70, 77284155, 'Meaux', 'Front de berge', 'LinÃ©aire R+1+C Ã  R+2. A noter particuliÃ¨rement du nÂ°72 et 74, 2 maisons de ville entre cour et jardin. Au nÂ°74, maison de ville en pierre de taille ; au nÂ°76, maison de bourg en meuliÃ¨re de style pittoresque.', 77),
(71, 78401152, 'Meulan-en-Yvelines', 'Pont Saint-CÃ´me', 'Pont en arc Ã  tablier portÃ© mÃ©tallique.', 78),
(72, 78498152, 'Poissy', 'Ancien Pont de Poissy', 'Il ne reste que cinq piles en pierres', 78),
(73, 92024152, 'Clichy-la-Garenne', 'Viaduc ferroviaire', 'JumelÃ© avec le pont routier, haut viaduc en bÃ©ton prÃ©contraint. Pont du mÃ©tro 13', 92),
(74, 91326151, 'Juvisy-sur-Orge', 'Pont de la premiÃ¨re armÃ©e franÃ§aise', 'Pont-poutres, tablier en bÃ©ton.', 91),
(75, 77533150, 'Vulaines-sur-Seine', 'Pont routier', 'Pont-poutres en bÃ©ton', 77),
(76, 77341150, 'Noyen-sur-Seine', 'Pont de la D49', 'Pont-poutres en bÃ©ton', 77),
(77, 94002150, 'Alfortville', 'Ancienne passerelle de service', 'Subsistent seulement les deux piles en bÃ©ton, encore prÃ©sentes sur chaque berge.', 94),
(78, 77284151, 'Meaux', 'Pont Jean Bureau', 'Pont-poutres en bÃ©ton Ã  double pile et trois travÃ©es', 77),
(79, 94068155, 'Saint-Maur-des-FossÃ©s', 'Pont du RER A', 'Pont-poutres Ã  trois travÃ©es, Ã  larges piles ; tablier mÃ©tallique, larges piles en pierre', 94),
(80, 94340152, 'Joinville-le-Pont', 'Viaduc de Joinville', 'Pont-poutres Ã  deux travÃ©es en bÃ©ton, murs anti-bruits vitrÃ©s', 94),
(81, 77475151, 'Trilport', 'Pont ferroviaire', 'Pont voÃ»tÃ© Ã  arcs de plein cintre Ã  3 travÃ©es et passage piÃ©ton sur les deux rives, avant-becs sur les piles ; bandeaux en pierres de taille, remplissage en briques', 77),
(82, 77084151, 'Changis-sur-Marne', 'Pont de la D53A', 'Pont-poutres Ã  une travÃ©e en bÃ©ton prÃ©fabriquÃ© et prÃ©contraint', 77),
(83, 77078150, 'Chamigny', 'Pont ferroviaire', 'Pont-poutres', 77),
(84, 78528150, 'Rolleboise', 'Front de berge', 'Au nÂ°3bis, intÃ©ressante maison de notable avec balcon central', 78),
(85, 77288156, 'Melun', 'Front de berge', 'Maisons de bourg et immeubles R+2 Ã  R+3, pierres de taille, enduit..', 77),
(86, 77419150, 'Saint-MammÃ¨s', 'Front de berge', 'Ensemble de maisons de bourg R+1 parfois R+1+C', 77),
(87, 75056153, 'Paris', 'Pont de Grenelle', 'Pont-poutres mÃ©tallique', 75),
(88, 75056154, 'Paris', 'Pont ferroviaire Rouelle', 'Structure diffÃ©renciÃ©e : rive gauche, pont voÃ»tÃ© en pierres ; rive droite, pont en arc en pierres, arche mÃ©tallique', 75),
(89, 75056168, 'Paris', 'Pont Saint-Michel', 'Pont voÃ»tÃ© Ã  arcs de plein cintre en pierre. EmblÃªme de NapolÃ©on au-dessus de chacune des piles.', 75),
(90, 75056170, 'Paris', 'Petit-Pont - Cardinal Lustiger', 'Pont voÃ»tÃ© en pierre meuliÃ¨re Ã  une arche surbaissÃ©e', 75),
(91, 75056172, 'Paris', 'Pont au Double', 'Pont en arc Ã  tablier portÃ© en fonte, arche unique revÃªtue de cuivre et culÃ©es en pierres de taille.', 75),
(92, 75056178, 'Paris', 'Pont de Sully', 'Pont en arc. Rive gauche, Ã  trois arches en fonte et piles en pierre ; rive droite Ã  arche centrale en fonte et deux en maÃ§onnerie.', 75),
(93, 75056184, 'Paris', 'Pont de Tolbiac', 'Pont voÃ»tÃ© en pierre Ã  arcs de plein cintre Ã  5 travÃ©es', 75),
(94, 78358151, 'Maisons-Laffitte', 'Pont ferroviaire', 'Pont voÃ»tÃ© Ã  arches de plein cintre en mÃ©tal et voÃ»tes en pierres.', 78),
(95, 78396151, 'Le Mesnil-le-Roi', 'Viaduc autoroutier de Montesson', 'Pont-poutres en deux sections, courbe, en bÃ©ton - Longueur : 501m de long', 78),
(96, 78146151, 'Chatou', 'Pont de Chatou', 'Pont-poutres Ã  tablier mÃ©tallique.', 78),
(97, 92012154, 'Boulogne-Billancourt', 'Pont DaydÃ©', 'Pont suspendu en bÃ©ton ; deux pylones en bÃ©ton sur lesquels reposent les suspensions.', 92),
(98, 77288153, 'Melun', 'Pont marÃ©chal de Lattre-de-Tassigny', 'Pont-poutres en bÃ©ton', 77),
(99, 77341151, 'Noyen-sur-Seine', 'Barrage-Ã©cluse du Vezoult', 'Barrage en bÃ©ton Ã  trois travÃ©es, Ã©cluse accolÃ©e', 77),
(100, 77243150, 'Lagny-sur-Marne', 'Pont de la D148', 'Pont en arc Ã  trois travÃ©es ;  arches en bÃ©ton (structure apparente), piles en pierre, culÃ©es en pierre meuliÃ¨re', 77),
(101, 92026153, 'Courbevoie', 'TÃªte de la passerelle piÃ©tonne', 'Immeubles R+8 Ã  gauche et R+9 Ã  droite adoptant le style paquebot avec terrasses en escalier.', 92),
(102, 75056163, 'Paris', 'Pont Royal', 'Pont voÃ»tÃ© en pierre Ã  arches de plein cintre.', 75),
(103, 92036150, 'Gennevilliers', 'Pont ferroviaire', 'Pont-poutres latÃ©rales Ã  treillis en mÃ©tal, piles en bÃ©ton.', 92),
(104, 75056183, 'Paris', 'Passerelle Simone-de-Beauvoir', 'Passerelle en acier Ã  structure arc-catÃ¨ne.', 75),
(105, 93031150, 'Epinay-sur-Seine', 'Pont ferroviaire', 'Pont voÃ»tÃ©, arches de plein cintre en pierres', 93),
(106, 92051151, 'Neuilly-sur-Seine', 'Pont de Neuilly', 'Pont en arc Ã  arches surbaissÃ©es mÃ©talliques, piles en pierres', 92),
(107, 94018151, 'Charenton-le-Pont', 'Pont Nelson Mandela', 'Pont-poutres en bÃ©ton', 94),
(108, 77188150, 'Fontaine-le-Port', 'Pont routier', 'Pont-poutres en bÃ©ton', 77),
(109, 77522150, 'Villiers-sur-Seine', 'Pont de Villiers', 'Pont-poutres en bÃ©ton', 77),
(110, 94046154, 'Maisons-Alfort', 'Passerelle de Charentonneau', 'Pont en arc en bÃ©ton Ã  tablier portÃ©, bombÃ© en son centre, Ã  deux travÃ©es', 94),
(111, 94052151, 'Nogent-sur-Marne', 'Viaduc de Nogent', 'Haut pont en arc Ã  nombreuses travÃ©es, Ã  tablier portÃ© ; une partie des arches sont en pierre, les autres en bÃ©ton armÃ©', 94),
(112, 77183151, 'La FertÃ©-sous-Jouarre', 'Pont Charles-de-Gaulle', 'Pont en arc Ã  tablier portÃ© Ã  trois travÃ©es en mÃ©tal (structure apparente), piles en pierres, arches surbaissÃ©es mÃ©talliques', 77),
(113, 77463150, 'Thomery', 'Front de berge', 'Les jardins sont sÃ©parÃ©s des habitations par la route. Quelques garages Ã  bateau y subsistent encore. Au nÂ°145, grande maison Ã  la faÃ§ade ornementÃ©e de briques tricolores, avec grande terrasse-solarium sur Seine.', 77),
(114, 78401150, 'Meulan-en-Yvelines', 'Pont Rhin et Danube', 'Pont-poutres Ã  tablier mÃ©tallique.', 78),
(115, 93070151, 'Saint-Ouen', 'Pont ferroviaire de Saint-Ouen', 'Pont-poutres latÃ©rales Ã  treillis mÃ©tallique, piles en pierres.', 93),
(116, 92026151, 'Courbevoie', 'Passerelle piÃ©tonne', 'Passerelle moderne', 92),
(117, 92012153, 'Boulogne-Billancourt', 'Pont Renault', 'Pont en arc, bombÃ©, partant de Boulogne-Billancourt - Longueur : 205m de long.', 92),
(118, 92012155, 'Boulogne-Billancourt', 'Pont de Billancourt', 'Deux ponts-poutres en acier, piles en bÃ©ton.', 92),
(119, 94018150, 'Charenton-le-Pont', 'Pont Nelson Mandela', 'Pont-poutres en bÃ©ton', 94),
(120, 77288152, 'Melun', 'Pont MarÃ©chal Leclerc', 'Pont-poutres en bÃ©ton', 77),
(121, 77389150, 'La Rochette', 'Pont ferroviaire du Pet-au-Diable', 'Pont-poutres, piles en pierre et tablier mÃ©tallique', 77),
(122, 77019150, 'Balloy', 'Pont routier de la D77', 'Pont-poutres en bÃ©ton', 77),
(123, 94046150, 'Maisons-Alfort', 'Pont ferroviaire de Charenton-Maisons-Alfort', 'Pont doublÃ©, Ã  3 travÃ©es ; en amont, pont en arc ; en aval, pont-poutres ; piles en pierre, tablier mÃ©tallique, voÃ»tes mÃ©talliques en amont Ã  arcs surbaisssÃ©s', 94),
(124, 77468150, 'Torcy', 'Pont de Torcy', 'Pont-poutres en bÃ©ton Ã  quatre travÃ©es, piles doubles', 77),
(125, 92004154, 'AsniÃ¨res-sur-Seine', 'Front de berge', '', 92),
(126, 92026154, 'Courbevoie', 'TÃªte du pont de Courbevoie', '', 92),
(127, 78172153, 'Conflans-Sainte-Honorine', 'Front de berge', '', 78),
(128, 77376150, 'PrÃ©cy-sur-Marne', 'Maison du passeur', 'BÃ¢timent avec pignon sur rue, R+1+C en enduit et incrustation de cÃ©ramique sur faÃ§ade', 77),
(129, 77126150, 'Congis-sur-ThÃ©rouanne', 'Usine Ã©lÃ©vatoire des eaux', 'BÃ¢timent R+C de plan rectangulaire', 77),
(130, 75056155, 'Paris', 'Pont de Bir-Hakeim', 'Pont-viaduc Ã  deux niveaux, pont en arc mÃ©tallique Ã  tablier portÃ©. Des sculptures en fonte ornent  les piles de pierre.', 75),
(131, 75056175, 'Paris', 'Pont Louis Philippe', 'Pont voÃ»tÃ© Ã  arcs surbaissÃ©s en pierre ; garde-corps en pierre et voie pavÃ©e.', 75),
(132, 75056185, 'Paris', 'Pont National', 'Pont voÃ»tÃ© en pierre meuliÃ¨re Ã  arches surbaissÃ©es.', 75),
(133, 75056157, 'Paris', 'Passerelle Debilly', 'Passerelle en arc, Ã  charpente mÃ©tallique, Ã  tablier intermÃ©diaire', 75),
(134, 78361152, 'Mantes-la-Jolie', 'Pont ferroviaire', 'Pont-poutres en bÃ©ton cÃ´tÃ© Mantes ; en pierre cÃ´tÃ© Limay.', 78),
(135, 78401151, 'Meulan-en-Yvelines', 'Pont aux Perches', 'Pont voÃ»tÃ© en pierre Ã  arcs en plein cintre. Le bras nord de la Seine a Ã©tÃ© comblÃ© et un jardin public le remplace.', 78),
(136, 78172151, 'Conflans-Sainte-Honorine', 'Viaduc de Conflans', 'Haut pont-poutres en bÃ©ton. Beau panorama sur la ville et sur ses quais plantÃ©s.', 78),
(137, 92024151, 'Clichy-la-Garenne', 'Pont de Clichy', 'Pont-poutres, jumelÃ© avec le pont ferroviaire en bÃ©ton prÃ©contraint', 92),
(138, 92051150, 'Neuilly-sur-Seine', 'Paserelle piÃ©tonne', 'Passerelle moderne Ã  double rampe; treillis mÃ©tallique', 92),
(139, 92062150, 'Puteaux', 'Pont de Puteaux', 'Pont-poutres en bÃ©ton - Longueur : 350m.', 92),
(140, 92048150, 'Meudon', 'Passerelle Seibert', 'Pont en poutre-treillis en bÃ©ton', 92),
(141, 77407150, 'Ponthierry-Saint-Fargeau', 'Pont du MarÃ©chal Juin', 'Pont-poutres en bÃ©ton.', 77),
(142, 77037150, 'Bois-le-Roi', 'Pont de la D115', 'Pont-poutres bombÃ© en bÃ©ton', 77),
(143, 77467150, 'Tombe (la)', 'Pont routier de la FertÃ© Gaucher', 'Pont-poutres en bÃ©ton', 77),
(144, 94017150, 'Champigny-sur-Marne', 'Pont de Champigny', 'Pont en arc en bÃ©ton Ã  tablier portÃ©, Ã  deux travÃ©es', 94),
(145, 94017151, 'Champigny-sur-Marne', 'Viaduc ferroviaire de Champigny', 'Pont-poutres Ã  treillis mÃ©tallique, piles en pierres, Ã  deux travÃ©es', 94),
(146, 93050150, 'Neuilly-sur-Marne', 'Viaduc de Neuilly-sur-Marne', 'Pont en arc en bÃ©ton Ã  trois travÃ©es, une seule enjambant la Marne', 93),
(147, 77231151, 'Isles-les-Meldeuses', 'Pont ferroviaire', 'Pont-poutres Ã  trois travÃ©es, piles en pierres de taille, tablier mÃ©tallique', 77),
(148, 77084150, 'Changis-sur-Marne', 'Pont ferroviaire', 'Pont voÃ»tÃ© en pierre Ã  quatre travÃ©es', 77),
(149, 77305155, 'Montereau-Fault-Yonne', 'Front de berge', 'Certaines maisons ont un double perron. Au nÂ°16, vaste maison de bourg avec double perron, faÃ§ade symÃ©trique et balcon central en R+1.', 77),
(150, 75056159, 'Paris', 'Pont des Invalides', 'Pont voÃ»tÃ© en pierre Ã  arcs surbaissÃ©s Ã  quatre travÃ©es. Scultpures sur les piles.', 75),
(151, 75056161, 'Paris', 'Pont de la Concorde', 'Pont voÃ»tÃ© Ã  arcs surbaissÃ©s en pierre', 75),
(152, 78358152, 'Maisons-Laffitte', 'Pont sur le Petit Bras', 'Pont Ã  une voie circulable, fermÃ© par un portail', 78),
(153, 93070150, 'Saint-Ouen', 'Pont de Saint-Ouen-les-Docks', 'Pont en arc en deux sections, Ã  arches surbaissÃ©es, en fonte et piles en pierres', 93),
(154, 92012151, 'Boulogne-Billancourt', 'Pont de Saint-Cloud', 'Pont-poutres Ã  tablier mÃ©tallique, piles en bÃ©ton et rambardes mÃ©talliques pleines - Longueur : 186m', 92),
(155, 94022152, 'Choisy-le-Roi', 'Pont de chemin de fer', 'Pont-poutres Ã  tablier mÃ©tallique et piles en bÃ©ton.', 94),
(156, 91174150, 'Corbeil-Essonnes', 'Viaduc de la N104', 'Pont-poutres double en bÃ©ton.', 91),
(157, 77305151, 'Montereau-Fault-Yonne', 'Pont Saint-Martin', 'Pont-poutres en bÃ©ton', 77),
(158, 94002152, 'Alfortville', 'Passerelle de service', 'Passerelle-poutres Ã  treillis mÃ©tallique', 94),
(159, 94068154, 'Saint-Maur-des-FossÃ©s', 'Pont de Bonneuil', 'Pont-poutres Ã  deux travÃ©e ; piles en bÃ©ton et tablier mÃ©tallique', 94),
(160, 77243151, 'Lagny-sur-Marne', 'Pont Maunoury', 'Pont en arc Ã  trois travÃ©es ; arches en bÃ©ton, piles en pierre, culÃ©es en pierre meuliÃ¨re', 77),
(161, 77284154, 'Meaux', 'Pont neuf', 'Pont-poutres Ã  trois travÃ©es en bÃ©ton. DÃ©cor : sculptures sur les piles du pont', 77),
(162, 91174153, 'Corbeil-Essonnes', 'Front de berge', 'Ensemble R+2 et R+2+C dont les jardins sont surÃ©levÃ©s par des murs de soutÃ¨nement.', 91),
(163, 77305154, 'Montereau-Fault-Yonne', 'Front de berge', 'Ensemble de maisons R+1+C, la plupart en enduit et pierres de taille avec modÃ©natures.', 77),
(164, 77369150, 'Poincy', 'Moulin de Poincy', 'Long bÃ¢timent principal R+1+C de plan rectangulaire', 77),
(165, 77183152, 'La FertÃ©-sous-Jouarre', 'Front de berge', 'Maisons R+1+C de styles variÃ©s (meuliÃ¨res, pierres de taille, ornementations, pittoresqueâ€¦). Rive droite, les jardins des maisons donnent directement sur Marne.', 77),
(166, 75056150, 'Paris', 'Pont Aval', 'Pont-poutres en bÃ©ton', 75),
(167, 75056164, 'Paris', 'Pont du Caroussel', 'Pont voÃ»tÃ© en pierre Ã  arches Ã  arcs surbaissÃ©s. 2 statues Ã  chaque extrÃ©mitÃ©.', 75),
(168, 75056162, 'Paris', 'Passerelle LÃ©opold-SÃ©dar-Sanghor', 'Passerelle en arc mÃ©tallique. Passerelle Ã  deux niveaux se rejoignant en partie centrale', 75),
(169, 78361151, 'Mantes-la-Jolie', 'Viaduc de la D983', 'Pont-poutres en bÃ©ton. Belle vue sur la collÃ©giale.', 78),
(170, 78624152, 'Triel-sur-seine', 'Viaduc routier', 'Pont-poutres Ã  double tablier mÃ©tallique. Pont de la rocade D1.', 78),
(171, 78481150, 'Le Pecq', 'Viaduc du chemin de fer', 'Pont-poutres Ã  treillis mÃ©tallique ; le pont se poursuit en viaduc au-dessus des quais rive gauche avec de hautes arches de plein cintre.', 78),
(172, 94077150, 'Villeneuve-le-Roi', 'Pont routier', 'Pont-poutres en bÃ©ton', 94),
(173, 78361154, 'Mantes-la-Jolie', 'Pont routier', 'Pont-poutres en bÃ©ton Ã  deux travÃ©es et une pile centrale', 78),
(174, 94068151, 'Saint-Maur-des-FossÃ©s', 'Pont de CrÃ©teil', 'Pont-poutres en bÃ©ton Ã  deux travÃ©es', 94),
(175, 94015150, 'Bry-sur-Marne', 'Pont de Bry', 'Pont-poutres en bÃ©ton Ã  une travÃ©e', 94),
(176, 93050151, 'Neuilly-sur-Marne', 'Pont de service', 'Pont-poutres en bÃ©ton Ã  trois travÃ©es, une seule enjambant la Marne', 93),
(177, 77475150, 'Trilport', 'Pont de la D603', 'Pont voÃ»tÃ© en pierre de taille Ã  arcs en plein cintre Ã  trois travÃ©es et avant-becs sur chacune des piles', 77),
(178, 77290150, 'MÃ©ry-sur-Marne', 'Pont de la D68A', 'Pont voÃ»tÃ© en pierre, remplissage en meuliÃ¨re, balustrades mÃ©talliques, Ã  trois travÃ©es, arcs de plein cintre, avant-becs', 77),
(179, 78358153, 'Maisons-Laffitte', 'TÃªte du pont de Maisons-Laffitte', 'ChÃ¢teau symÃ©trique avec aile centrale et 2 ailes latÃ©rales, toiture mansardÃ©e. Jardins Ã  la franÃ§aise avec taupiÃ¨res et arbres en rideaux.', 78),
(180, 78410150, 'Moisson (Lavacourt)', 'Front de berge', 'La plupart des maisons sont en moellons en appareil rÃ©gulier dressÃ© et enduit. Au nÂ°9, auberge \"au rendez-vous des canotiers\"', 78),
(181, 95257150, 'La Frette-sur-Seine', 'Front de berge', 'Mairie au nÂ°55 et Ã©glise du XVIe siÃ¨cle au 48.', 95);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
