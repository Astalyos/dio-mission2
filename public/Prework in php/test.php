<?php
  $leJSon = file_get_contents("patrimoine.json");    //Obtient le fichier json
  $parsed_json = json_decode($leJSon,true);          //Parse le Json
  $tabFields = array();                              //Creer un tableau vide 

  foreach ($parsed_json as $value) {               
   $tabFields[] = $value['fields'];
  }

  //print_r($tabFields[0]);
  //print("<br /><br /><br />");

  $tabfinal = array();


  for ($i=0;$i<sizeof($tabFields);$i++) {

   $tabtri = array();


   $commune = $tabFields[$i]['commune'];
   $id = $tabFields[$i]['identifian'];
   $elem_patri = $tabFields[$i]['elem_patri'];
   if(empty($tabFields[$i]['elem_princ'])){
    $elem_princ=null;
   }
   else{
     $elem_princ = $tabFields[$i]['elem_princ'];
   }
   $tabtri = [
     "identifian"=>$id, "commune"=>$commune, "elem_patri"=>$elem_patri, "elem_princ"=>$elem_princ, "departement"=>substr($id,0,2)
   ];
   
   $tabfinal[$i] = $tabtri;
  }

  $ecriture = fopen('ordonnedjson.json','w+');
  fwrite($ecriture, json_encode($tabfinal,JSON_UNESCAPED_UNICODE |
  JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
  fclose($ecriture);

  var_dump($tabfinal);

 ?>