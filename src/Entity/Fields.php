<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ApiFilter(SearchFilter::class, properties={"id": "exact", "identifian": "exact", "commune": "exact", "elem_patri": "", "elem_princ": "partial", "departement": "partial"})
 * @ORM\Entity(repositoryClass="App\Repository\FieldsRepository")
 */
class Fields
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $identifian;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $commune;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $elem_patri;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $elem_princ;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $departement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdentifian(): ?int
    {
        return $this->identifian;
    }

    public function setIdentifian(int $identifian): self
    {
        $this->identifian = $identifian;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(string $commune): self
    {
        $this->commune = $commune;

        return $this;
    }

    public function getElemPatri(): ?string
    {
        return $this->elem_patri;
    }

    public function setElemPatri(?string $elem_patri): self
    {
        $this->elem_patri = $elem_patri;

        return $this;
    }

    public function getElemPrinc(): ?string
    {
        return $this->elem_princ;
    }

    public function setElemPrinc(?string $elem_princ): self
    {
        $this->elem_princ = $elem_princ;

        return $this;
    }

    public function getDepartement(): ?int
    {
        return $this->departement;
    }

    public function setDepartement(?int $departement): self
    {
        $this->departement = $departement;

        return $this;
    }
}
